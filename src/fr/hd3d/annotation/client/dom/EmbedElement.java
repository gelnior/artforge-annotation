package fr.hd3d.annotation.client.dom;

import com.google.gwt.dom.client.TagName;
import com.google.gwt.user.client.Element;


@TagName(EmbedElement.TAG)
public class EmbedElement extends Element
{

    public static final String TAG = "embed";

    protected EmbedElement()
    {}

    public static EmbedElement as(Element element)
    {
        assert element.getTagName().equalsIgnoreCase(TAG);
        return (EmbedElement) element;
    }

    public final native void setSrc(String src)/*-{
		this.src = src;
    }-*/;

    public final native String getSrc()/*-{
		return this.src;
    }-*/;

    public final native void setType(String type)/*-{
		this.type = type;
    }-*/;

    public final native String getType()/*-{
		return this.type;
    }-*/;

    public final native void setAllowsciptaccess(boolean allowScriptAccess)/*-{
		this.allowsciptaccess = allowScriptAccess;
    }-*/;

    public final native void setAllowfullscreen(boolean allowFullScreen)/*-{
		this.allowfullscreen = allowFullScreen;
    }-*/;

}
