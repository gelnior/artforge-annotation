package fr.hd3d.annotation.client.view;

import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.annotation.client.player.AnnotationPlayer;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.mainview.MainView;
import fr.hd3d.common.ui.client.widget.proxyplayer.ProxyPlayer;


public class HD3DAnnotationView extends MainView
{

    private HD3DAnnotationController annotationController;

    public HD3DAnnotationView()
    {
        super("HD3D Annotation");
        annotationController = new HD3DAnnotationController(new MainModel(), this);
        EventDispatcher.get().addController(annotationController);
    }

    @Override
    public void init()
    {
        ProxyModelData proxyVideoModelData = new ProxyModelData();
        proxyVideoModelData.setId(new Long(1));
        proxyVideoModelData.setProxyLocation("http://localhost:8181/hd3dannotation/test_medium.mp4");
        proxyVideoModelData.setProxyTypeOpenType("video/mp4");
        
//        ProxyModelData proxyVideoModelData2 = new ProxyModelData();
//        proxyVideoModelData2.setId(new Long(5));
//        proxyVideoModelData2.setProxyLocation("http://localhost:8181/hd3dannotation/video2.mp4");
//        proxyVideoModelData2.setProxyTypeOpenType(ProxyMimeType.VIDEO_MP4.getType());
        
//        List<ProxyModelData> proxies = new ArrayList<ProxyModelData>();
//        proxies.add(proxyVideoModelData);
//        proxies.add(proxyVideoModelData2);
        
//        ProxyPlayer proxyPlayer = new ProxyPlayer(proxyVideoModelData);
//        proxyPlayer.setPixelSize(500, 400);
        
        AnnotationPlayer annotationPlayer = new AnnotationPlayer(proxyVideoModelData);
        annotationPlayer.setPixelSize(500, 400);
        
//        RootPanel.get().add(proxyPlayer);
        RootPanel.get().add(annotationPlayer);
        
    }

}
