package fr.hd3d.annotation.client.view;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.mainview.MainController;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;

public class HD3DAnnotationController extends MainController
{

    private final MainModel model;
    private final HD3DAnnotationView view;

    public HD3DAnnotationController(MainModel model, HD3DAnnotationView view)
    {
        super(model, view);
        this.model = model;
        this.view = view;
    }
    
    @Override
    protected void onSettingInitialized(AppEvent event)
    {
        this.view.hideStartPanel();
        this.view.init();
    }

}
