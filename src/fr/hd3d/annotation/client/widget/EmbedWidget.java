package fr.hd3d.annotation.client.widget;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Widget;

import fr.hd3d.annotation.client.dom.EmbedElement;


public class EmbedWidget extends Widget
{

    private EmbedElement embedElement;

    public EmbedWidget(String src, String type)
    {
        this.embedElement = EmbedElement.as(DOM.createElement(EmbedElement.TAG));
        setElement(this.embedElement);
        this.embedElement.setSrc(src);
        this.embedElement.setType(type);
    }

    public void setAllowScriptAcess(boolean allowScriptAccess)
    {
        this.embedElement.setAllowsciptaccess(allowScriptAccess);
    }

    public void setAllowFullScreen(boolean allowFullScreen)
    {
        this.embedElement.setAllowfullscreen(allowFullScreen);
    }

}
