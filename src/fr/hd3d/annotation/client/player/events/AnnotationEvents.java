package fr.hd3d.annotation.client.player.events;

import com.extjs.gxt.ui.client.event.EventType;

public class AnnotationEvents
{
    public static final EventType RECT_TOOL_SELECTED_EVENT = new EventType();
    public static final EventType CIRCLE_TOOL_SELECTED_EVENT = new EventType();
    public static final EventType MARKUP_EVENT = new EventType();
    public static final EventType REMOVE_MARKUP_EVENT = new EventType();
    public static final EventType NONE_TOOL_SELECTED_EVENT = new EventType();
    public static final EventType ANNOTATION_SELECTED_EVENT = new EventType();
    public static final EventType REMOVE_SELECTED_ANNOTATION_EVENT = new EventType();
    public static final EventType SAVE_ANNOTATION_EVENT = new EventType();
    public static final EventType PREPARE_NEXT_ANNOTATION_EVENT = new EventType();
    public static final EventType SHOW_ANNOTATION_DIALOG_EVENT = new EventType();
    public static final EventType ADD_ANNOTATION_TO_HISTORY_EVENT = new EventType();
    public static final EventType SET_FILE_REVISION_TO_ANNOTATION_EVENT = new EventType();
    public static final EventType GRAPHICAL_ANNOTATION_SAVED_EVENT = new EventType();
}
