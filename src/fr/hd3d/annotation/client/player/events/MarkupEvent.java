package fr.hd3d.annotation.client.player.events;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.annotation.client.player.wrappers.SVGShapeWrapper;
import fr.hd3d.html5.svg.client.user.ui.SVGGraphicalWidget;


public class MarkupEvent extends AppEvent
{
    private SVGShapeWrapper markup;
    private SVGGraphicalWidget graphicalWidget;
    private boolean controlDown;

    public MarkupEvent(boolean selection)
    {
        super(AnnotationEvents.MARKUP_EVENT);
        if (selection)
        {
            setType(AnnotationEvents.ANNOTATION_SELECTED_EVENT);
        }
    }

    public SVGShapeWrapper getMarkup()
    {
        return markup;
    }

    public void setMarkup(SVGShapeWrapper markup)
    {
        this.markup = markup;
    }

    public SVGGraphicalWidget getGraphicalWidget()
    {
        return graphicalWidget;
    }

    public void setGraphicalWidget(SVGGraphicalWidget graphicalWidget)
    {
        this.graphicalWidget = graphicalWidget;
    }

    public void setControlDown(boolean ctrlPress)
    {
        this.controlDown = ctrlPress;
    }

    public boolean isControlDown()
    {
        return controlDown;
    }

}
