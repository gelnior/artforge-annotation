package fr.hd3d.annotation.client.player;

import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.user.client.ui.Widget;

import fr.hd3d.annotation.client.player.annotation.AnnotationToolBar;
import fr.hd3d.annotation.client.player.controller.AnnotationController;
import fr.hd3d.annotation.client.player.events.MarkupEvent;
import fr.hd3d.annotation.client.player.model.AnnotationModel;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.widget.proxyplayer.util.ProxyMimeType;
//import fr.hd3d.html5.video.client.VideoWidget.TypeSupport;


public class AnnotationPlayer extends ContentPanel
{
    private AnnotationToolBar annotationToolBar;

    private final AnnotationController annotationPlayerController;

    private final AnnotationModel annotationModel;

    private final ProxyModelData proxyModelData;

    private IAnnotationPlayer displayer;

    public AnnotationPlayer(ProxyModelData proxyModelData)
    {
        super(new FitLayout());
        this.proxyModelData = proxyModelData;
        this.annotationModel = new AnnotationModel();
        // this.annotationToolBar = new AnnotationToolBar(annotationModel);
        annotationPlayerController = new AnnotationController(this, annotationModel);

        // this.setTopComponent(this.annotationToolBar);
        addProxyPlayer();
        this.setHeaderVisible(false);
        EventDispatcher.get().addController(annotationPlayerController);
    }

    public void removeControllers()
    {
        if (this.annotationPlayerController != null)
        {
            EventDispatcher.get().removeController(this.annotationPlayerController);
            displayer.removeControllers();
        }
    }

    private void addProxyPlayer()
    {
        if (isVideoType(proxyModelData))
        {
            try
            {
//                displayer = new AnnotationVideoPlayer();
                if (!isTypeSupported(proxyModelData))
                {
                    displayer = new AnnotationDownloadProxy();
                }
            }
            catch (Exception e)
            {}
        }
        else if (isImage(proxyModelData))
        {
            displayer = new AnnotationImageViewer();
        }
        else
        {
            displayer = new AnnotationDownloadProxy();
        }

        if (displayer != null && displayer instanceof Widget)
        {
            if (proxyModelData.getDownloadPath() != null)
            {
                displayer.setSrc(proxyModelData.getDownloadPath());
            }
            this.displayer.setAnnotationModel(annotationModel);
            this.add((Widget) this.displayer);
        }
    }

    private boolean isTypeSupported(ProxyModelData proxyModelData)
    {
//         TypeSupport canPlayType = ((AnnotationVideoPlayer) displayer).getVideoHTML().canPlayType(
//         proxyModelData.getProxyTypeOpenType());
//         return !TypeSupport.NO.equals(canPlayType);
    	return false;
    }

    @Override
    protected void onResize(int width, int height)
    {
        super.onResize(width, height);
        if (displayer != null && displayer instanceof Widget)
        {
            int fw = this.getInnerWidth();
            int fh = this.getInnerHeight();
            ((Widget) displayer).setPixelSize(fw, fh);
        }

    }

    public String getBorderColor()
    {
        return this.annotationToolBar.getBorderColorButton().getSelectedColor();
    }

    public String getBackgroundColor()
    {
        return this.annotationToolBar.getBackGroundColorButton().getSelectedColor();
    }

    public void setMarkup(MarkupEvent markupEvent)
    {
        this.displayer.setMarkup(markupEvent);
    }

    public void removeMarkup()
    {
        this.displayer.removeMarkup();
    }

    public void selectAnnotation(MarkupEvent markupEvent)
    {
        this.displayer.selectAnnotation(markupEvent);
    }

    public IAnnotationPlayer getProxyPlayer()
    {
        return displayer;
    }

    public boolean getFill()
    {
        return this.annotationToolBar.getFill();
    }

    private boolean isImage(ProxyModelData proxyModelData)
    {
        return ProxyMimeType.IMAGE_GIF.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.IMAGE_JPEG.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.IMAGE_PNG.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType());

    }

    private boolean isVideoType(ProxyModelData proxyModelData)
    {
        return ProxyMimeType.VIDEO_MP4.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.VIDEO_OGG.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.VIDEO_WEBM.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType());
    }

    public IAnnotationPlayer getDisplayer()
    {
        return displayer;
    }

}
