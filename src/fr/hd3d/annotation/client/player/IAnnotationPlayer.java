package fr.hd3d.annotation.client.player;

import fr.hd3d.annotation.client.player.events.MarkupEvent;
import fr.hd3d.annotation.client.player.model.AnnotationModel;
import fr.hd3d.common.ui.client.widget.proxyplayer.IProxyPlayer;

public interface IAnnotationPlayer extends IProxyPlayer
{
    public void setAnnotationModel(AnnotationModel annotationModel);
    public void setMarkup(MarkupEvent markupEvent);
    public void removeMarkup();
    public void selectAnnotation(MarkupEvent markupEvent);
    public int getPlayerOffsetWidth();
    public int getPlayerOffsetHeight();
    public double getCurrentTime();
    public double getDuration();
    public void setFrameRate(int framerate);
}
