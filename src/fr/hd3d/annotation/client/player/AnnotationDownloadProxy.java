package fr.hd3d.annotation.client.player;

import fr.hd3d.annotation.client.player.events.MarkupEvent;
import fr.hd3d.annotation.client.player.model.AnnotationModel;
import fr.hd3d.common.ui.client.widget.proxyplayer.DownloadProxy;


public class AnnotationDownloadProxy extends DownloadProxy implements IAnnotationPlayer
{

    @Override
    public void setSrc(String src)
    {
        this.setHTML(MESSAGES.downloadProxyAnnotation(src));
    }
    
    @Override
    public double getCurrentTime()
    {
        return 0;
    }

    @Override
    public double getDuration()
    {
        return 0;
    }

    @Override
    public int getPlayerOffsetHeight()
    {
        return getOffsetHeight();
    }

    @Override
    public int getPlayerOffsetWidth()
    {
        return getOffsetWidth();
    }

    @Override
    public void removeMarkup()
    {}

    @Override
    public void selectAnnotation(MarkupEvent markupEvent)
    {}

    @Override
    public void setAnnotationModel(AnnotationModel annotationModel)
    {}

    @Override
    public void setMarkup(MarkupEvent markupEvent)
    {}

    @Override
    public void setFrameRate(int framerate)
    {
    }

}
