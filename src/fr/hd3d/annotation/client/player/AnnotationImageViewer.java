package fr.hd3d.annotation.client.player;

import java.util.List;

import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.AbsoluteData;
import com.extjs.gxt.ui.client.widget.layout.AbsoluteLayout;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.SimplePanel;

import fr.hd3d.annotation.client.player.annotation.AnnotationLayer;
import fr.hd3d.annotation.client.player.events.MarkupEvent;
import fr.hd3d.annotation.client.player.model.AnnotationModel;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.widget.proxyplayer.ImageViewer;


public class AnnotationImageViewer extends FocusPanel implements IAnnotationPlayer
{

    protected ImageViewer imageViewer;
    protected AnnotationLayer annotationLayer;
    protected SimplePanel focusPanel;
    protected ContentPanel contentPanel;

    public AnnotationImageViewer()
    {
        super();
        AbsoluteData absoluteData = new AbsoluteData(0, 0);
        imageViewer = new ImageViewer();
        contentPanel = new ContentPanel(new AbsoluteLayout());
        focusPanel = new SimplePanel();
        annotationLayer = new AnnotationLayer();

        focusPanel.getElement().getStyle().setBottom(0, Unit.PX);
        focusPanel.getElement().getStyle().setRight(0, Unit.PX);
        focusPanel.add(annotationLayer);
        contentPanel.add(imageViewer, absoluteData);
        contentPanel.add(focusPanel, absoluteData);

        this.contentPanel.setHeaderVisible(false);
        this.contentPanel.setBodyBorder(false);
        this.contentPanel.setBorders(false);

        this.add(contentPanel);
    }

    @Override
    public void setPixelSize(int width, int height)
    {
        super.setPixelSize(width, height);
        this.contentPanel.setPixelSize(width, height);
    }

    @Override
    public int getPlayerOffsetHeight()
    {
        return contentPanel.getOffsetHeight();
    }

    @Override
    public int getPlayerOffsetWidth()
    {
        return contentPanel.getOffsetWidth();
    }

    @Override
    public void removeMarkup()
    {
        this.annotationLayer.removeMarkup();
    }

    @Override
    public void selectAnnotation(MarkupEvent markupEvent)
    {
        this.annotationLayer.selectAnnotation(markupEvent);
    }

    @Override
    public void setAnnotationModel(AnnotationModel annotationModel)
    {
        this.annotationLayer.setAnnotationModel(annotationModel);
    }

    @Override
    public void setMarkup(MarkupEvent markupEvent)
    {
        this.annotationLayer.setMarkup(markupEvent);
    }

    @Override
    public double getCurrentTime()
    {
        return 0;
    }

    @Override
    public double getDuration()
    {
        return 0;
    }

    @Override
    public void setSrc(String src)
    {
        if (this.imageViewer != null)
        {
            this.imageViewer.setSrc(src);
        }
    }

    @Override
    public void removeControllers()
    {
    }

    @Override
    public void setPlayList(List<ProxyModelData> proxyModelDatas)
    {
    }

    @Override
    public void setFrameRate(int framerate)
    {
    }

}
