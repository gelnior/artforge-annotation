package fr.hd3d.annotation.client.player.controller;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.annotation.client.player.AnnotationPlayer;
import fr.hd3d.annotation.client.player.events.AnnotationEvents;
import fr.hd3d.annotation.client.player.events.MarkupEvent;
import fr.hd3d.annotation.client.player.model.AnnotationModel;
import fr.hd3d.annotation.client.player.wrappers.SVGShapeWrapper;
import fr.hd3d.common.ui.client.widget.proxyplayer.events.VideoPlayerEvent;


public class AnnotationController extends Controller
{

    private AnnotationPlayer annotationPlayer;
    private final AnnotationModel annotationModel;

    public AnnotationController(AnnotationPlayer annotationPlayer, AnnotationModel annotationModel)
    {
        this.annotationPlayer = annotationPlayer;
        this.annotationModel = annotationModel;
        registerEvents();
    }

    private void registerEvents()
    {
        this.registerEventTypes(AnnotationEvents.RECT_TOOL_SELECTED_EVENT);
        this.registerEventTypes(AnnotationEvents.CIRCLE_TOOL_SELECTED_EVENT);
        this.registerEventTypes(AnnotationEvents.NONE_TOOL_SELECTED_EVENT);
        this.registerEventTypes(AnnotationEvents.MARKUP_EVENT);
        this.registerEventTypes(AnnotationEvents.ANNOTATION_SELECTED_EVENT);
        this.registerEventTypes(AnnotationEvents.REMOVE_MARKUP_EVENT);
        this.registerEventTypes(AnnotationEvents.REMOVE_SELECTED_ANNOTATION_EVENT);
        this.registerEventTypes(AnnotationEvents.SAVE_ANNOTATION_EVENT);
        this.registerEventTypes(AnnotationEvents.PREPARE_NEXT_ANNOTATION_EVENT);
        this.registerEventTypes(VideoPlayerEvent.VIDEO_TIME_UPDATE_EVENT);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        // boolean fill = this.annotationPlayer.getFill();
        // String borderColor = this.annotationPlayer.getBorderColor();
        // String backgroundColor = (fill) ? this.annotationPlayer.getBackgroundColor() : "none";
        // if (AnnotationEvents.RECT_TOOL_SELECTED_EVENT.equals(event.getType()))
        // {
        // this.annotationModel.setSvgWidget(new SVGRectWrapper(borderColor, backgroundColor));
        // this.annotationModel.setToolEvent(event);
        // }
        // else if (AnnotationEvents.CIRCLE_TOOL_SELECTED_EVENT.equals(event.getType()))
        // {
        // this.annotationModel.setSvgWidget(new SVGCircleWrapper(borderColor, backgroundColor));
        // this.annotationModel.setToolEvent(event);
        // }
        // else if (AnnotationEvents.NONE_TOOL_SELECTED_EVENT.equals(event.getType()))
        // {
        // this.annotationModel.setSvgWidget(null);
        // this.annotationModel.setToolEvent(null);
        // }
        if (AnnotationEvents.PREPARE_NEXT_ANNOTATION_EVENT.equals(event.getType())) {
            this.annotationModel.prepareNextAnnotation();
        }
        else if (AnnotationEvents.MARKUP_EVENT.equals(event.getType()))
        {
            MarkupEvent markupEvent = (MarkupEvent) event;
            this.annotationPlayer.setMarkup(markupEvent);
        }
        else if (AnnotationEvents.ANNOTATION_SELECTED_EVENT.equals(event.getType()))
        {
            MarkupEvent markupEvent = (MarkupEvent) event;
            if (this.annotationModel.getAnnotationList().indexOf(markupEvent.getGraphicalWidget()) != -1)
            {
                deselectAnnotation(markupEvent);
            }
            else
            {
                this.annotationPlayer.selectAnnotation(markupEvent);
            }
        }
        else if (AnnotationEvents.REMOVE_MARKUP_EVENT.equals(event.getType()))
        {
            this.annotationPlayer.removeMarkup();
        }
        else if (AnnotationEvents.REMOVE_SELECTED_ANNOTATION_EVENT.equals(event.getType()))
        {
            this.annotationModel.removeSelectedAnnotation();
        }
        else if (AnnotationEvents.SAVE_ANNOTATION_EVENT.equals(event.getType()))
        {
            onSaveAnnotation(event);
        }
        else if (VideoPlayerEvent.VIDEO_TIME_UPDATE_EVENT.equals(event.getType())) {
            showAnnotations();
        }
    }

    private void showAnnotations()
    {
        // long currentTime = (long) (this.annotationPlayer.getVideoPlayer().getCurrentTime() * 1000);
    }

    private void onSaveAnnotation(AppEvent event)
    {
        SVGShapeWrapper shapeWrapper = (SVGShapeWrapper) event.getData();
        if (shapeWrapper != null)
        {
            // GraphicalAnnotationModelData annotationModelData = new GraphicalAnnotationModelData();
            // long markin = (long) (this.annotationPlayer.getVideoPlayer().getCurrentTime() * 1000);
            // long markout = (long) (this.annotationPlayer.getVideoPlayer().getDuration() * 1000);
            // annotationModelData.setAnnotationSvg(shapeWrapper.getGraphicalWidget().toString());
            // annotationModelData.setMarkIn(Long.valueOf(markin));
            // annotationModelData.setMarkOut(Long.valueOf(markout));
            // annotationModelData.setDate(new Date());
            // annotationModelData.setFileRevision(Long.valueOf(1));
            // annotationModelData.save();
        }
    }

    private void deselectAnnotation(MarkupEvent markupEvent)
    {
        if (markupEvent.getGraphicalWidget() != null)
        {
            annotationModel.removeAnnotation(markupEvent);
        }
    }

}
