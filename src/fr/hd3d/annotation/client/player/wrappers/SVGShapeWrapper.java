package fr.hd3d.annotation.client.player.wrappers;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;

import fr.hd3d.annotation.client.player.annotation.AnnotationLayer;
import fr.hd3d.annotation.client.player.events.AnnotationEvents;
import fr.hd3d.annotation.client.player.events.MarkupEvent;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.html5.svg.client.user.ui.SVGGraphicalWidget;


public abstract class SVGShapeWrapper implements MouseOutHandler, MouseOverHandler, MouseUpHandler
{
    protected static final Unit PX = Unit.PX;
    protected static int ID = 1;
    protected static final int MOUSE_OUT_STROKE_WIDTH = 2;
    protected static final int SELECTED_MARKUP_STROKE_WIDTH = 3;
    protected static final String SELECTED_COLOR = "#99bbe8";
    private String borderColor;
    private String backgroundColor;
    private SVGGraphicalWidget graphicalWidget;
    private SVGShapeWrapper selectionMarkup;
    private final boolean dynamiqueRender;

    public SVGShapeWrapper(String borderColor, String backgroundColor, boolean dynmiqueRender)
    {
        this.borderColor = borderColor;
        this.backgroundColor = backgroundColor;
        this.dynamiqueRender = dynmiqueRender;
    }

    public abstract void setX(float x);

    public abstract float getX();

    public abstract void setY(float y);

    public abstract float getY();

    public abstract void setWidth(float width);

    public abstract int getWidth();

    public abstract void setHeight(float height);

    public abstract int getHeight();

    public abstract SVGGraphicalWidget createSVGWidget();

    public abstract SVGShapeWrapper createSelectedMarkup();

    public void setListeners()
    {
        graphicalWidget.addMouseOverHandler(this);
        graphicalWidget.addMouseOutHandler(this);
        graphicalWidget.addMouseUpHandler(this);
    }

    public String getBorderColor()
    {
        return borderColor;
    }

    public void setBorderColor(String borderColor)
    {
        this.borderColor = borderColor;
    }

    public String getBackgroundColor()
    {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor)
    {
        this.backgroundColor = backgroundColor;
    }

    private void showSelectedMarkup(SVGGraphicalWidget baseSVGWidget, boolean selection, boolean ctrlPress)
    {
        MarkupEvent markupEvent = new MarkupEvent(selection);
        SVGShapeWrapper createSelectedMarkup = createSelectedMarkup();
        markupEvent.setMarkup(createSelectedMarkup);
        markupEvent.setGraphicalWidget(baseSVGWidget);
        markupEvent.setControlDown(ctrlPress);
        if (selection)
        {
            this.selectionMarkup = createSelectedMarkup;
        }
        EventDispatcher.forwardEvent(markupEvent);
    }

    public void setGraphicalWidget(SVGGraphicalWidget graphicalWidget)
    {
        this.graphicalWidget = graphicalWidget;
    }

    public SVGGraphicalWidget getGraphicalWidget()
    {
        return graphicalWidget;
    }

    public SVGShapeWrapper getSelectionMarkup()
    {
        return selectionMarkup;
    }
    
    public boolean isDynamiqueRender()
    {
        return dynamiqueRender;
    }
    
    @Override
    public void onMouseOut(MouseOutEvent event)
    {
        EventDispatcher.forwardEvent(AnnotationEvents.REMOVE_MARKUP_EVENT);
    }
    
    @Override
    public void onMouseOver(MouseOverEvent event)
    {
        SVGGraphicalWidget baseSVGWidget = (SVGGraphicalWidget) event.getSource();
        AnnotationLayer owner = (AnnotationLayer) baseSVGWidget.getParent();
        if (baseSVGWidget != null && owner != null && !owner.isDrawing())
        {
            showSelectedMarkup(baseSVGWidget, false, false);
        }
    }

    @Override
    public void onMouseUp(MouseUpEvent event)
    {
        SVGGraphicalWidget baseSVGWidget = (SVGGraphicalWidget) event.getSource();
        AnnotationLayer owner = (AnnotationLayer) baseSVGWidget.getParent();
        if (baseSVGWidget != null && owner != null && !owner.isDrawing())
        {
            showSelectedMarkup(baseSVGWidget, true, event.isControlKeyDown());
        }
    }

}
