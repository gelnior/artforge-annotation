package fr.hd3d.annotation.client.player.wrappers;

import fr.hd3d.html5.svg.client.dom.PresentationStyle.PointerEvents;
import fr.hd3d.html5.svg.client.user.ui.SVGGraphicalWidget;
import fr.hd3d.html5.svg.client.user.ui.SVGRectWidget;


public class SVGRectWrapper extends SVGShapeWrapper
{

    private SVGRectWidget rectWidget;

    public SVGRectWrapper(String borderColor, String backgroundColor)
    {
        super(borderColor, backgroundColor, true);
    }

    @Override
    public void setHeight(float height)
    {
        this.rectWidget.setHeight((int) height, PX);
    }

    @Override
    public void setWidth(float width)
    {
        this.rectWidget.setWidth((int) width, PX);
    }

    @Override
    public void setX(float x)
    {
        this.rectWidget.setX(x, PX);
    }

    @Override
    public void setY(float y)
    {
        this.rectWidget.setY(y, PX);
    }

    @Override
    public SVGGraphicalWidget createSVGWidget()
    {
        this.rectWidget = new SVGRectWidget(0, 0, 0, 0, PX);
        setGraphicalWidget(rectWidget);
        rectWidget.getPresentationStyle().setStroke(getBorderColor());
        rectWidget.getPresentationStyle().setStrokeWidth(MOUSE_OUT_STROKE_WIDTH, PX);
        rectWidget.getPresentationStyle().setFill(getBackgroundColor());
        rectWidget.getPresentationStyle().setPointerEvents(PointerEvents.ALL);
        rectWidget.getElement().setId("" + (ID++));
        return this.rectWidget;
    }

    @Override
    public SVGShapeWrapper createSelectedMarkup()
    {
        SVGRectWrapper rectWrapper = new SVGRectWrapper(SELECTED_COLOR, "none");
        SVGGraphicalWidget markup = rectWrapper.createSVGWidget();
        markup.getPresentationStyle().setStrokeWidth(SELECTED_MARKUP_STROKE_WIDTH, PX);
        markup.getPresentationStyle().setPointerEvents(PointerEvents.NONE);
        rectWrapper.setX(getX() - SELECTED_MARKUP_STROKE_WIDTH);
        rectWrapper.setY(getY() - SELECTED_MARKUP_STROKE_WIDTH);
        rectWrapper.setWidth(getWidth() + SELECTED_MARKUP_STROKE_WIDTH * 2);
        rectWrapper.setHeight(getHeight() + SELECTED_MARKUP_STROKE_WIDTH * 2);
        return rectWrapper;
    }

    @Override
    public int getHeight()
    {
        return (int) rectWidget.getHeight().getBaseVal().getValue();
    }

    @Override
    public int getWidth()
    {
        return (int) rectWidget.getWidth().getBaseVal().getValue();
    }

    @Override
    public float getX()
    {
        return rectWidget.getX().getBaseVal().getValue();
    }

    @Override
    public float getY()
    {
        return rectWidget.getY().getBaseVal().getValue();
    }
}
