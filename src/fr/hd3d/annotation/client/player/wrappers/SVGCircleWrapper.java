package fr.hd3d.annotation.client.player.wrappers;

import com.google.gwt.dom.client.Style.Unit;

import fr.hd3d.html5.svg.client.dom.PresentationStyle.PointerEvents;
import fr.hd3d.html5.svg.client.user.ui.SVGEllipseWidget;
import fr.hd3d.html5.svg.client.user.ui.SVGGraphicalWidget;


public class SVGCircleWrapper extends SVGShapeWrapper
{

    private SVGEllipseWidget circleWidget;
    private boolean xInit = false;
    private boolean yInit = false;

    public SVGCircleWrapper(String borderColor, String backgroundColor)
    {
        super(borderColor, backgroundColor, true);
    }

    @Override
    public void setHeight(float height)
    {
        this.circleWidget.setRy((int) height, PX);
    }

    @Override
    public void setWidth(float width)
    {
        this.circleWidget.setRx((int) width, PX);
    }

    @Override
    public void setX(float x)
    {
        if (!xInit) {
            this.circleWidget.setCx((int) x, PX);
            xInit = true;
        }
    }

    @Override
    public void setY(float y)
    {
        if (!yInit) {
            this.circleWidget.setCy((int) y, PX);
            yInit = true;
        }
    }

    @Override
    public SVGGraphicalWidget createSVGWidget()
    {
        this.circleWidget = new SVGEllipseWidget(0, 0, 0, 0, PX);
        setGraphicalWidget(circleWidget);
        circleWidget.getPresentationStyle().setStroke(getBorderColor());
        circleWidget.getPresentationStyle().setStrokeWidth(MOUSE_OUT_STROKE_WIDTH, Unit.PX);
        circleWidget.getPresentationStyle().setFill(getBackgroundColor());
        circleWidget.getPresentationStyle().setPointerEvents(PointerEvents.ALL);
        circleWidget.getElement().setId("" + (ID++));
        return this.circleWidget;
    }

    @Override
    public void setListeners()
    {
        circleWidget.addMouseOverHandler(this);
        circleWidget.addMouseOutHandler(this);
        circleWidget.addMouseUpHandler(this);
    }

    @Override
    public SVGShapeWrapper createSelectedMarkup()
    {
        SVGCircleWrapper circleWrapper = new SVGCircleWrapper(SELECTED_COLOR, "none");
        SVGGraphicalWidget markup = circleWrapper.createSVGWidget();
        markup.getPresentationStyle().setStrokeWidth(SELECTED_MARKUP_STROKE_WIDTH, PX);
        markup.getPresentationStyle().setPointerEvents(PointerEvents.NONE);
        circleWrapper.setX(getX());
        circleWrapper.setY(getY());
        circleWrapper.setWidth(getWidth() + SELECTED_MARKUP_STROKE_WIDTH);
        circleWrapper.setHeight(getHeight() + SELECTED_MARKUP_STROKE_WIDTH); 
        return circleWrapper;
    }

    @Override
    public int getHeight()
    {
        return (int) (circleWidget.getRy().getBaseVal().getValue());
    }

    @Override
    public int getWidth()
    {
        return (int) (circleWidget.getRx().getBaseVal().getValue());
    }

    @Override
    public float getX()
    {
        return circleWidget.getCx().getBaseVal().getValue();
    }

    @Override
    public float getY()
    {
        return circleWidget.getCy().getBaseVal().getValue();
    }

}
