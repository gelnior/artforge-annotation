package fr.hd3d.annotation.client.player;

import com.extjs.gxt.ui.client.widget.layout.AbsoluteData;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.SimplePanel;

import fr.hd3d.annotation.client.player.annotation.AnnotationLayer;
import fr.hd3d.annotation.client.player.events.AnnotationEvents;
import fr.hd3d.annotation.client.player.events.MarkupEvent;
import fr.hd3d.annotation.client.player.model.AnnotationModel;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.proxyplayer.VideoPlayer;


public class AnnotationVideoPlayer //extends VideoPlayer implements IAnnotationPlayer
{
    protected AnnotationLayer annotationLayer;
    protected SimplePanel focusPanel;

    public AnnotationVideoPlayer()
    {
//        this(false, null, getDefaultResources());
    }

    public AnnotationVideoPlayer(boolean autoPlay, String poster)//, Resources resources)
    {
//        super(autoPlay, poster, resources, true);
        AbsoluteData absoluteData = new AbsoluteData(0, 0);
        focusPanel = new SimplePanel();
        focusPanel.getElement().getStyle().setBottom(0, Unit.PX);
        focusPanel.getElement().getStyle().setRight(0, Unit.PX);
        annotationLayer = new AnnotationLayer();
        focusPanel.add(annotationLayer);
//        contentPanel.add(focusPanel, absoluteData);
//        contentPanel.setHeaderVisible(false);
//        contentPanel.setBodyBorder(false);
//        contentPanel.setBorders(false);
    }

//    @Override
//    protected void addVideoEvents()
//    {
//        super.addVideoEvents();
//        this.addKeyUpHandler(new KeyUpHandler() {
//
//            public void onKeyUp(KeyUpEvent event)
//            {
//                int keyCode = event.getNativeKeyCode();
//                if (KeyCodes.KEY_DELETE == keyCode)
//                {
//                    EventDispatcher.forwardEvent(AnnotationEvents.REMOVE_SELECTED_ANNOTATION_EVENT);
//                }
//            }
//        });
//    }

    public void setAnnotationModel(AnnotationModel annotationModel)
    {
        this.annotationLayer.setAnnotationModel(annotationModel);
    }

    public void setMarkup(MarkupEvent markupEvent)
    {
        this.annotationLayer.setMarkup(markupEvent);
    }

    public void removeMarkup()
    {
        this.annotationLayer.removeMarkup();
    }

    public void selectAnnotation(MarkupEvent markupEvent)
    {
        this.annotationLayer.selectAnnotation(markupEvent);
    }

//    public int getPlayerOffsetHeight()
//    {
//        return getContentPanel().getOffsetHeight();
//    }
//
//    public int getPlayerOffsetWidth()
//    {
//        return getContentPanel().getOffsetWidth();
//    }
//
//    public double getCurrentTime()
//    {
//        return getVideoHTML().getCurrentTime();
//    }
//
//    public double getDuration()
//    {
//        return getVideoHTML().getDuration();
//    }
//
//    @Override
//    public void setFrameRate(int framerate)
//    {
//        super.setFrameRate(framerate);
//    }

}
