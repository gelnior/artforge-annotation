package fr.hd3d.annotation.client.player.model;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.annotation.client.player.events.MarkupEvent;
import fr.hd3d.annotation.client.player.wrappers.SVGRectWrapper;
import fr.hd3d.annotation.client.player.wrappers.SVGShapeWrapper;
import fr.hd3d.html5.svg.client.user.ui.SVGGraphicalWidget;


public class AnnotationModel
{
    private SVGShapeWrapper svgWidget;
    private SVGGraphicalWidget markup;
    private AppEvent toolEvent;
    private List<SVGShapeWrapper> selectionMarkupList;
    private List<SVGGraphicalWidget> annotationList;
    private static final String DEFAULT_BG_COLOR = "none";
    private static final String DEFAULT_BORDER_COLOR = "#2f4f4f";
    
    public AnnotationModel()
    {
        prepareNextAnnotation();
    }
    
    public SVGShapeWrapper getSvgWidget()
    {
        return svgWidget;
    }

    public void setSvgWidget(SVGShapeWrapper svgWidget)
    {
        this.svgWidget = svgWidget;
    }

    public void setMarkup(SVGGraphicalWidget markup)
    {
        this.markup = markup;
    }

    public SVGGraphicalWidget getMarkup()
    {
        return markup;
    }

    public void setToolEvent(AppEvent event)
    {
        this.toolEvent = event;
    }

    public AppEvent getToolEvent()
    {
        return toolEvent;
    }

    public List<SVGShapeWrapper> getSelectionMarkupList()
    {
        if (selectionMarkupList == null)
        {
            selectionMarkupList = new ArrayList<SVGShapeWrapper>();
        }
        return selectionMarkupList;
    }

    public List<SVGGraphicalWidget> getAnnotationList()
    {
        if (annotationList == null)
        {
            annotationList = new ArrayList<SVGGraphicalWidget>();
        }
        return annotationList;
    }

    public void removeAnnotation(MarkupEvent markupEvent)
    {
        int annotationIndex = getAnnotationList().indexOf(markupEvent.getGraphicalWidget());
        if (annotationIndex != -1)
        {
            SVGShapeWrapper shapeWrapper = getSelectionMarkupList().get(annotationIndex);
            if (shapeWrapper != null)
            {
                shapeWrapper.getGraphicalWidget().removeFromParent();
            }
            getAnnotationList().remove(annotationIndex);
            getSelectionMarkupList().remove(annotationIndex);
        }
    }

    public void removeSelectedAnnotation()
    {
        for (SVGGraphicalWidget graphicalWidget : getAnnotationList())
        {
            graphicalWidget.removeFromParent();
        }
        for (SVGShapeWrapper shapeWrapper : getSelectionMarkupList())
        {
            shapeWrapper.getGraphicalWidget().removeFromParent();
        }
        markup.removeFromParent();
        annotationList.clear();
        selectionMarkupList.clear();
    }

    public void prepareNextAnnotation()
    {
        this.svgWidget = new SVGRectWrapper(DEFAULT_BORDER_COLOR, DEFAULT_BG_COLOR);
    }
}
