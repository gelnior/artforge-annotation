package fr.hd3d.annotation.client.player.annotation;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.button.ToggleButton;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.menu.ColorMenu;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

import fr.hd3d.annotation.client.player.events.AnnotationEvents;
import fr.hd3d.annotation.client.player.model.AnnotationModel;
import fr.hd3d.common.ui.client.event.EventDispatcher;


public class AnnotationToolBar extends ToolBar
{
//    private static Resources DEFAULT_RESOURCES;
    private static final String TOOGLE_BUTTON_GROUP = "toolsButtonGroup";
//    private Resources resources;
    private AnnotationToolBarButton rectBtn;
    private AnnotationToolBarButton circleBtn;
    private ColorButton borderColorButton;
    private ColorButton backGroundColorButton;
    private CheckBox fillCheckBox;
    private final AnnotationModel annotationModel;

//    private static Resources getDefaultResources()
//    {
//        if (DEFAULT_RESOURCES == null)
//        {
//            DEFAULT_RESOURCES = GWT.create(Resources.class);
//        }
//        return DEFAULT_RESOURCES;
//    }

//    public static interface Resources extends ClientBundle
//    {
//        ImageResource square();
//
//        ImageResource circle();
//
//        ImageResource text();
//
//    }

    public AnnotationToolBar(AnnotationModel annotationModel)
    {
        super();
        this.annotationModel = annotationModel;
//        this.resources = getDefaultResources();
//        rectBtn = new AnnotationToolBarButton(AnnotationEvents.RECT_TOOL_SELECTED_EVENT, "Square", resources.square());
//        circleBtn = new AnnotationToolBarButton(AnnotationEvents.CIRCLE_TOOL_SELECTED_EVENT, "Circle", resources
//                .circle());
        borderColorButton = new ColorButton();
        backGroundColorButton = new ColorButton();
        fillCheckBox = new CheckBox();
        fillCheckBox.setBoxLabel("Fill");

        borderColorButton.setWidth(20);
        borderColorButton.setToolTip("Border Color");
        borderColorButton.setColor("#000000");
        backGroundColorButton.setWidth(20);
        backGroundColorButton.setToolTip("Background Color");
        backGroundColorButton.setColor("#FFFFFF");
        fillCheckBox.setToolTip("Unckeck if you don't want shape's background");

//        this.add(rectBtn);
//        this.add(circleBtn);
        this.add(borderColorButton);
        this.add(backGroundColorButton);
        this.add(fillCheckBox);
        setListeners();
    }

    private void setListeners()
    {
        setColorButtonListener(this.backGroundColorButton);
        setColorButtonListener(this.borderColorButton);
        this.fillCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
        
            @Override
            public void handleEvent(FieldEvent be)
            {
                if (annotationModel.getToolEvent() != null)
                {
                    EventDispatcher.forwardEvent(annotationModel.getToolEvent());
                }        
            }
        });
    }

    private void setColorButtonListener(ColorButton colorButton)
    {
        ColorMenu menu = (ColorMenu) colorButton.getMenu();
        menu.getColorPalette().addListener(Events.Select, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent be)
            {
                if (annotationModel.getToolEvent() != null)
                {
                    EventDispatcher.forwardEvent(annotationModel.getToolEvent());
                }
            }
        });
    }

    @Override
    public boolean add(Component item)
    {
        if (item instanceof ToggleButton)
        {
            ((ToggleButton) item).setToggleGroup(TOOGLE_BUTTON_GROUP);
        }
        return super.add(item);
    }
    
    public ColorButton getBorderColorButton()
    {
        return borderColorButton;
    }

    public ColorButton getBackGroundColorButton()
    {
        return backGroundColorButton;
    }

    public boolean getFill()
    {
        return fillCheckBox.getValue();
    }

}
