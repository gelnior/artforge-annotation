package fr.hd3d.annotation.client.player.annotation;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.ColorPalette;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.menu.ColorMenu;


public class ColorButton extends Button
{
    private static String COLOR_TO_REPLACE = "{color}";
    private static String TEMPLATE = "<div style='width:100%; height:100%; text-align:center; background-color:"
            + COLOR_TO_REPLACE + ";'>" + "&nbsp;" + "</div>";
    private String selectedColor = null;

    public ColorButton()
    {
        super(replaceTemplate("#FFFFFF"));
        ColorMenu menu = new ColorMenu();
        String[] colors = menu.getColorPalette().getColors();
        colors[colors.length - 1] = "";
        menu.getColorPalette().addListener(Events.Select, new Listener<ComponentEvent>() {

            public void handleEvent(ComponentEvent be)
            {
                selectedColor = ((ColorPalette) be.getComponent()).getValue();
                setText(replaceTemplate(selectedColor));
            }
        });

        setMenu(menu);
        setToolTip("Click for change color");
        setWidth(30);
    }

    public void setColor(String color)
    {
        setText(replaceTemplate(color));
        this.selectedColor = color;
    }

    private static String replaceTemplate(String color)
    {
        if (!color.startsWith("#"))
        {
            color = "#" + color;
        }
        return TEMPLATE.replace(COLOR_TO_REPLACE, color);
    }

    public String getSelectedColor()
    {
        if (selectedColor != null)
        {
            if (!selectedColor.startsWith("#"))
            {
                selectedColor = "#" + selectedColor;
            }
        }
        return selectedColor;
    }

}
