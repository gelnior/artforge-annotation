package fr.hd3d.annotation.client.player.annotation;

import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.user.client.DOM;

import fr.hd3d.annotation.client.player.events.AnnotationEvents;
import fr.hd3d.annotation.client.player.events.MarkupEvent;
import fr.hd3d.annotation.client.player.model.AnnotationModel;
import fr.hd3d.annotation.client.player.wrappers.SVGShapeWrapper;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.html5.svg.client.user.ui.BaseSVGWidget;
import fr.hd3d.html5.svg.client.user.ui.SVGGraphicalWidget;
import fr.hd3d.html5.svg.client.user.ui.SVGWidget;


public class AnnotationLayer extends SVGWidget implements MouseDownHandler, MouseUpHandler, MouseMoveHandler
{
    private static final int DRAGGABLE_DISTANCE = 2;
    private boolean isDrawing = false;
    private SVGShapeWrapper baseSVGWidget;
    private float startX;
    private float startY;
    private AnnotationModel annotationModel;
    private int parentTop = 0;
    private int parentLeft = 0;

    public AnnotationLayer()
    {
        addDomHandler(this, MouseDownEvent.getType());
        addDomHandler(this, MouseUpEvent.getType());
        addDomHandler(this, MouseMoveEvent.getType());
    }

    public void setAnnotationModel(AnnotationModel annotationModel)
    {
        this.annotationModel = annotationModel;
    }

    @Override
    public void onMouseDown(MouseDownEvent event)
    {
        this.baseSVGWidget = annotationModel.getSvgWidget();
        if (this.baseSVGWidget != null)
        {
            parentLeft = this.getParent().getAbsoluteLeft();
            parentTop = this.getParent().getAbsoluteTop();
            startX = event.getClientX();
            startY = event.getClientY();
        }
    }

    @Override
    public void onMouseUp(MouseUpEvent event)
    {
        if (isDrawing && baseSVGWidget != null)
        {
            setListenerAndPrepareNextAnnotation();
        }
        isDrawing = false;
        baseSVGWidget = null;
    }

    private void setListenerAndPrepareNextAnnotation()
    {
        baseSVGWidget.setListeners();
        // if (annotationModel.getToolEvent() != null)
        // {
        EventDispatcher.forwardEvent(AnnotationEvents.SHOW_ANNOTATION_DIALOG_EVENT, baseSVGWidget);
        EventDispatcher.forwardEvent(AnnotationEvents.PREPARE_NEXT_ANNOTATION_EVENT);
        // EventDispatcher.forwardEvent(this.annotationModel.getToolEvent());
        // }
    }

    @Override
    public void onMouseMove(MouseMoveEvent event)
    {
        int x = event.getClientX();
        int y = event.getClientY();
        int deltaX = (int) (x - startX);
        int deltaY = (int) (y - startY);
        if (!isDrawing && baseSVGWidget != null && baseSVGWidget.isDynamiqueRender()
                && (Math.abs(deltaX) > DRAGGABLE_DISTANCE || Math.abs(deltaY) > DRAGGABLE_DISTANCE))
        {
            isDrawing = true;
            createAndAddAnnotation();
        }
        if (isDrawing && baseSVGWidget != null)
        {
            if (deltaX < 0 && deltaY > 0)
            {
                baseSVGWidget.setX(x - parentLeft);
            }
            else if (deltaX < 0 && deltaY < 0)
            {
                baseSVGWidget.setX(x - parentLeft);
                baseSVGWidget.setY(y - parentTop);
            }
            else if (deltaX > 0 && deltaY < 0)
            {
                baseSVGWidget.setY(y - parentTop);
            }
            baseSVGWidget.setWidth(Math.abs(deltaX));
            baseSVGWidget.setHeight(Math.abs(deltaY));

        }
    }

    private void createAndAddAnnotation()
    {
        BaseSVGWidget widget = baseSVGWidget.createSVGWidget();
        baseSVGWidget.setX(startX - parentLeft);
        baseSVGWidget.setY(startY - parentTop);
        if (widget != null)
        {
            this.add(widget);
        }
    }

    public void setMarkup(MarkupEvent markupEvent)
    {
        removeMarkup();
        SVGGraphicalWidget markup = markupEvent.getMarkup().getGraphicalWidget();
        annotationModel.setMarkup(markup);
        int widgetIndex = this.getWidgetIndex(markupEvent.getGraphicalWidget());
        if (widgetIndex != -1)
        {
            this.add(markup);
            DOM.insertChild(getElement(), markup.getElement(), widgetIndex + 1);
        }
    }

    public void removeMarkup()
    {
        if (this.annotationModel.getMarkup() != null)
        {
            this.annotationModel.getMarkup().removeFromParent();
        }
    }

    public boolean isDrawing()
    {
        return this.isDrawing;
    }

    public void removeSelection()
    {
        for (SVGShapeWrapper shapeWrapper : annotationModel.getSelectionMarkupList())
        {
            shapeWrapper.getGraphicalWidget().removeFromParent();
        }
        this.annotationModel.getSelectionMarkupList().clear();
        this.annotationModel.getAnnotationList().clear();
    }

    public void selectAnnotation(MarkupEvent markupEvent)
    {
        if (!markupEvent.isControlDown())
        {
            removeSelection();
        }
        SVGGraphicalWidget selectedWidget = markupEvent.getMarkup().getGraphicalWidget();
        int widgetIndex = this.getWidgetIndex(markupEvent.getGraphicalWidget());
        if (widgetIndex != -1)
        {
            this.add(selectedWidget);
            this.annotationModel.getSelectionMarkupList().add(markupEvent.getMarkup());
            this.annotationModel.getAnnotationList().add(markupEvent.getGraphicalWidget());
            DOM.insertChild(getElement(), selectedWidget.getElement(), widgetIndex + 1);
        }
    }
}
