package fr.hd3d.annotation.client.player.annotation;

import com.extjs.gxt.ui.client.Style.ButtonScale;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.button.ToggleButton;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

import fr.hd3d.annotation.client.player.events.AnnotationEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;


public class AnnotationToolBarButton extends ToggleButton
{
    public AnnotationToolBarButton(final EventType eventType, String tooltip, ImageResource icon)
    {
        super();
        setIcon(AbstractImagePrototype.create(icon));
        setScale(ButtonScale.MEDIUM);
        setToolTip(tooltip);
        addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                if (isPressed())
                {
                    EventDispatcher.forwardEvent(new AppEvent(eventType, AnnotationToolBarButton.this));
                }
                else
                {
                    EventDispatcher.forwardEvent(AnnotationEvents.NONE_TOOL_SELECTED_EVENT);
                }
            }
        });
    }
}
