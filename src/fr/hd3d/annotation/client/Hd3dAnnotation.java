package fr.hd3d.annotation.client;

import com.google.gwt.core.client.EntryPoint;

import fr.hd3d.annotation.client.view.HD3DAnnotationView;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Hd3dAnnotation implements EntryPoint
{
    public void onModuleLoad()
    {
        new HD3DAnnotationView();
        EventDispatcher.forwardEvent(CommonEvents.START);
    }
}
